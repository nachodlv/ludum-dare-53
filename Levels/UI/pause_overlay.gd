extends Control

var showed_continue : bool = false

func _process(delta):
	if (Input.is_action_just_pressed("pause")):
		($TextureButton as TextureButton).toggle_mode = true
		($TextureButton as TextureButton).set_pressed_no_signal(true)
		
	if (Input.is_action_just_released("pause")):
		($TextureButton as TextureButton).set_pressed_no_signal(false)
		($TextureButton as TextureButton).toggle_mode = false
		toggle_pause()
	
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE if visible else Input.MOUSE_MODE_HIDDEN)

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	if (!GameState.showed_continue_button):
		toggle_pause()
		$ContinueButton.visible = true
		$TextureButton.visible = false
		GameState.showed_continue_button = true
		showed_continue = true
	
func _exit_tree():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
func toggle_pause():
	if (showed_continue):
		$ContinueButton.visible = false
		$TextureButton.visible = true
	get_tree().paused = !get_tree().paused
	visible = get_tree().paused
