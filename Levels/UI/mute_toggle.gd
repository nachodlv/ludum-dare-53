extends Control

func _ready():
	var master_sound = AudioServer.get_bus_index("Master")
	_change_texture(!AudioServer.is_bus_mute(master_sound))

func toggle_mute():
	var master_sound = AudioServer.get_bus_index("Master")
	var is_mute = AudioServer.is_bus_mute(master_sound)
	AudioServer.set_bus_mute(master_sound, !is_mute)
	_change_texture(is_mute)

func _change_texture(sound_on : bool):
	if sound_on:
		$TextureRect.texture = load("res://Sprites/Sonido.png")
	else:
		$TextureRect.texture = load("res://Sprites/Mute.png")
