extends Node

enum CollisionMask
{ 
	COL_MASK_INTERACTAREA = 1 << 0,
	COL_MASK_OBJ_PACKAGE = 1 << 1,
	COL_MASK_OBJ_SEAL = 1 << 2,
	COL_MASK_OBJ_PACKAGE_CONVEYOR = 1 << 3,
	COL_MASK_OBJ_ALL = 14,
	COL_MASK_ALL = 15
}

var _rng = RandomNumberGenerator.new()
var _canvas : CanvasItem

static func get_current(contextNode: Node):
	return contextNode.get_node("/root/LDHelper")

func set_collisions(target_object : CollisionObject2D, collision_sets):
	for i in range(collision_sets.size()):
		target_object.set_collision_layer_value(collision_sets[i][0], collision_sets[i][1])
		target_object.set_collision_mask_value(collision_sets[i][0], collision_sets[i][1])

func move_to_closestpoint(target_object : RigidBody2D, target_area : InteractableArea) -> Vector2:
	var shape : Shape2D = target_area.shape_owner_get_shape(target_area.shape_find_owner(0), 0)
	var bounds = shape.get_rect()
	bounds.position = target_area.global_position
	#If its inside we consider its already at the closest point
	if bounds.has_point(target_object.global_transform.origin):
		return Vector2()
	
	var extents : Vector2 = bounds.size/2
	var object_pos : Vector2 = target_object.global_transform.origin
	var closest_point = find_closestpoint_to_rect2D(object_pos, target_area.global_position, extents)
	target_object.global_transform.origin = closest_point
	return closest_point
	

func find_closestpoint_to_rect2D(point : Vector2, target_center : Vector2, extents : Vector2):
	var target_pos = point
	var clamp_pos_max : Vector2 = target_center + extents
	var clamp_pos_min : Vector2 = target_center - extents
	
	if (target_pos.x < clamp_pos_min.x) || (target_pos.x > clamp_pos_max.x):
		target_pos.x = clamp_pos_min.x if (target_pos.x < target_center.x) else clamp_pos_max.x
	
	if (target_pos.y < clamp_pos_min.y) || (target_pos.y > clamp_pos_max.y):
		target_pos.y = clamp_pos_min.y if (target_pos.y < target_center.y) else clamp_pos_max.y
		
	return target_pos

func get_singlelayerindex_from_mask(collision_mask_value : int):
	for i in 33:
		if collision_mask_value % 2 == 1:
			return i + 1
		else:
			collision_mask_value /= 2
	return 0

func get_random_from_weighted_array(elements : Array):
	var total_weight = 0
	for element in elements:
		total_weight += element.weight
	var random_weight = _rng.randf_range(0, total_weight) as float
	total_weight = 0
	for element in elements:
		total_weight += element.weight
		if random_weight < total_weight:
			return element
	return null
