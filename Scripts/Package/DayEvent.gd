extends Resource

class_name DayEvent

@export var needed_packages : Array[PackageTemplate]
@export var mission_start : Array[Mission]
