extends Node

@export var seconds_per_day : float = 60
@export var score_objective : float = 3500
@export var max_days = 4
@export var skipable_days_debug : Array[int] = []

#Turn this on if you're testing in any other level (not unlimited time tho)
@export var is_test_level : bool = false

signal score_change (old_score : float, new_score : float, score_discounts : float)
signal day_change (new_day : int)
signal wrong_package_sent(new_day : float)
signal day_ended
signal package_sent (package : Package)
signal package_destroyed (package : Package)
signal package_shelved (package : Package)
signal mission_added (new_mission_reward)

var time_remaining : float = 0

var showed_continue_button : bool = false

var day_score : float
var day_discounts : float
var _day_ended = false
var missions_rewards : Array
var _ambience_player : AudioStreamPlayer
var _game_started = false
var _instruction_displayer : InstructionDisplayer

var main_area : InteractableArea

var current_day : int = 0:
	set (new_day):
		if (current_day != new_day):
			current_day = new_day

var total_score : float = 0 : 
	set (new_score):
		if (total_score != new_score):
			var old_score = total_score
			total_score = new_score
			var score_delta = new_score - old_score
			day_score += score_delta if (score_delta > 0) else 0
			score_change.emit(old_score, new_score, day_discounts)

var events_per_day = load("res://Events/events_per_day.tres") as EventsPerDay

static func get_current(contextNode: Node):
	return contextNode.get_node("/root/GameState")

func start_day():
	current_day += 1
	if (current_day != 0):
		get_tree().change_scene_to_file("res://Levels/test_level_nacho.tscn")
		await get_tree().current_scene.tree_exited
		await get_tree().process_frame
	day_change.emit(current_day)
	time_remaining = seconds_per_day
	_day_ended = false
	day_score = 0
	day_discounts = 0
	var scene_node : Node2D = get_tree().root.get_child(3)
	_instruction_displayer = scene_node.find_child("InstructionDisplayer")
	_instruction_displayer.set_displays(current_day - 1)
	missions_rewards.clear()
	

func end_day():
	if (_day_ended):
		return
	_day_ended = true
	total_score += day_discounts
	for child in get_tree().root.get_child(3).get_children():
		if child is Package:
			destroy_package(child)
	get_tree().change_scene_to_file("res://Levels/end_of_day.tscn")
	await get_tree().current_scene.tree_exited
	await get_tree().process_frame
	day_ended.emit()
	
func _ready():
	var scene_node : Node2D = get_tree().root.get_child(3)
	if scene_node.name != "MainMenu":
		start_game()
	# TODO this should probably change
	if (is_test_level):
		time_remaining = 120

	var master_sound = AudioServer.get_bus_index("Master")
	AudioServer.set_bus_volume_db(master_sound, -10)


func start_game():
	_game_started = true
	start_day() 
	_ambience_player = AudioStreamPlayer.new()
	self.add_child(_ambience_player)
	_ambience_player.stream = load("res://SFX/Ambience_Warehouse.mp3")
	_ambience_player.volume_db = -10
	_ambience_player.play()

func _process(delta):
	if !_game_started:
		return
	time_remaining -= delta
	if (time_remaining <= 0 || skipable_days_debug.has(current_day)):
		end_day()

func send_package(package : Package) :
	package_sent.emit(package)
	total_score += 80 + 30 * package.get_seal_count()

func destroy_package(package : Package):
	package_destroyed.emit(package)

func send_wrong_package(package : Package):
	package_destroyed.emit(package)
	day_discounts -= 50
	wrong_package_sent.emit(day_discounts)
	score_change.emit(total_score, total_score, day_discounts)

func shelve_package(package : Package):
	package_shelved.emit(package)

func add_mission_reward(mission_reward):
	missions_rewards.append(mission_reward)
	total_score += mission_reward.score
	mission_added.emit(mission_reward)

func reset():
	current_day = 0
	total_score = 0
	time_remaining = 0
	day_score = 0
	day_discounts = 0
	_day_ended = false
	missions_rewards = []
	_game_started = false
	MissionManager.reset()
	_ambience_player.stop()
	showed_continue_button = false

		
