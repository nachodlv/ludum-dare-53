extends CharacterBody2D

var _object_held : CollisionObject2D

@export var right_input_name : String
@export var left_input_name : String
@export var up_input_name : String
@export var down_input_name : String
@export var interact_name : String
@export var flip_name : String
@export var apply_seal_name : String

@export var use_mouse : bool = false

@export var speed : Vector2 = Vector2(1,1)

@export var current_drawer : InteractableDrawer;

@export var top_left_limit : Vector2 = Vector2(0,0)
@export var bottom_right_limit : Vector2 = Vector2(1920,1080)

@export var collision : CollisionShape2D
@export var sprite : Sprite2D
@export var hand_pivot : Node2D
@export var interactable_component : InteractableComponent

var is_left : bool;

signal package_grabbed
signal package_dropped
signal seal_grabbed

var current_object_layer : int

func _ready():
	is_left = up_input_name.contains("left")

func _physics_process(delta):
	move_from_input()
	handle_drawer_hover()
	handle_pickup()
	handle_apply_seal()
	handle_package_flip()
	clamp_to_playspace()

func handle_package_flip():
	if (_object_held is Package && Input.is_action_just_pressed(flip_name)):
		(_object_held as Package).revert()
	pass
	
func handle_drawer_hover():
	if (!_object_held || !(_object_held is Package)):
		if (current_drawer):
			current_drawer.has_hover = false
			current_drawer.set_status(false)
			current_drawer = null
		return
	
	var area = interactable_component.get_interactable_atpos(position, [InteractableDrawer])
	
	if (area):
		current_drawer = (area as InteractableDrawer)
		current_drawer.has_hover = true
		
		
	if (!area && current_drawer):
		current_drawer.has_hover = false
		current_drawer.set_status(false)
		current_drawer = null
		

func handle_apply_seal():
	var seal_held = _object_held as Seal
	if (seal_held && Input.is_action_just_pressed(apply_seal_name)):
		var package = interactable_component.get_interactable_atpos(position, [Package])
		if (package):
			package.try_apply_seal(seal_held)
	pass

func handle_pickup():
	var possible_object = interactable_component.get_interactable_atpos(position, [Seal, Package], LdHelper.CollisionMask.COL_MASK_OBJ_ALL)
	var is_interacting = Input.get_action_strength(interact_name) != 0
	var wants_to_interact = Input.is_action_just_pressed(interact_name)
	
	if (wants_to_interact && !_object_held && possible_object):
		_object_held = possible_object
		current_object_layer = LdHelper.get_singlelayerindex_from_mask(possible_object.collision_mask) 
		LdHelper.set_collisions(_object_held, [[current_object_layer, false],[5, true]])
		if _object_held is Package:
			_object_held.rb_physics_enabled = false
			package_grabbed.emit()
		elif _object_held is Seal:
			seal_grabbed.emit()
		
	if (_object_held):
		if (!is_interacting):
			# Release
			handle_release()
			LdHelper.set_collisions(_object_held, [[current_object_layer, true],[5, false]])
			current_object_layer = 0
			_object_held = null
		else:
			handle_object_follow()
			
	if _object_held:
		sprite.texture = load("res://Sprites/Mano_1_Cerrada.png")
		if (_object_held is Package):
			_object_held.on_pickup.emit(_object_held)
		
	elif possible_object:
		sprite.texture = load("res://Sprites/Mano_1_Abierta.png")
	else:
		sprite.texture = load("res://Sprites/Mano_1.png")

func handle_release():
	var processed = false
	if (_object_held is Package):
		var area = interactable_component.get_interactable_atpos(position, [InteractableArea])
		(_object_held as Package).rb_physics_enabled = true
		(_object_held as Package).linear_velocity = velocity
		
		if (area):
			processed = (area as InteractableArea).solve_interaction(_object_held)
		else:
			LdHelper.move_to_closestpoint(_object_held, GameState.main_area)
	
	if (_object_held is Seal):
		(_object_held as Seal).return_to_start()
		
	if (!processed):
		package_dropped.emit()

func handle_object_follow():
	var offset = Vector2(0,0)
	if (_object_held is Package):
		var package = _object_held as Package
		var pivot = package.left_hand_pivot if is_left else package.right_hand_pivot
		offset = pivot.global_transform.origin - package.global_transform.origin
	_object_held.global_transform.origin = hand_pivot.global_position - offset

func move_from_input():
	var input_vector 
	if (use_mouse):
		var mouse_pos = get_global_mouse_position()
		input_vector = mouse_pos - global_transform.origin
		global_transform.origin = mouse_pos - hand_pivot.position
	else:
		input_vector = Vector2(
			Input.get_action_strength(right_input_name) - Input.get_action_strength(left_input_name),
			Input.get_action_strength(down_input_name) - Input.get_action_strength(up_input_name)
		)
		input_vector = input_vector.normalized()
		velocity = input_vector * speed
		move_and_slide()

func clamp_to_playspace():
	var collision_halfsize = collision.shape.get_rect().size / 2;
	var top_left_limit_pos = top_left_limit * get_viewport_rect().size
	var bottom_right_limit_pos = bottom_right_limit * get_viewport_rect().size
	
	position = position.clamp(
		top_left_limit_pos + collision_halfsize, 
		bottom_right_limit_pos - collision_halfsize)
