extends Node2D

class_name PackageSpawner

@export var time_between_spawns_per_day : Array[float]
@export var min_wait_between_spawns : float = 3
@export var regular_packages : Array[PackageTemplate]

var mission_packages : Array[PackageTemplate]

var _package = load("res://Levels/Package/package.tscn")
var _timmy_package = load("res://Levels/Package/timmy_package.tscn")
var _mafia_package = load("res://Levels/Package/hand_package.tscn")
var _hogwarts_package = load("res://Levels/Package/hogwarts_package.tscn")

var _special_packages_with_time : Array
var _rng = RandomNumberGenerator.new()
var _game_state : GameState

func _ready():
	_game_state = GameState.get_current(self) as GameState
	var events = _game_state.events_per_day.events as Array[DayEvent]
	await get_tree().process_frame
	# second parameters is needed so this timer is paused during pause
	await get_tree().create_timer(1, false).timeout
	if (events.size() >= _game_state.current_day):
		_calculate_special_packages_time(events[_game_state.current_day - 1].needed_packages)
	call_deferred("_spawn")
	
	
func _spawn():
	var conveyor = get_node("/root/Node2D/Conveyor") as Conveyor
	if (conveyor.has_available_slots()):
		var package_template = _get_package_resource() as PackageTemplate
		var new_package = _package
		if package_template.timmy_package:
			new_package = _timmy_package
		elif package_template.mafia_hand:
			new_package = _mafia_package
		elif package_template.hogwarts:
			new_package = _hogwarts_package
		new_package = new_package.instantiate()
		await get_tree().root.get_child(3).call_deferred("add_child", new_package)
		new_package.package = package_template
		new_package.save_original_collision_size()
		conveyor.add_package_to_conveyor(new_package)
		print("Spawning package: " + new_package.package.resource_path)
	var next_spawn = _next_time_for_spawn()
	if (next_spawn > 0):
		print("Waiting " + str(next_spawn).pad_decimals(0) + "s for next spawn")
		# second parameters is needed so this timer is paused during pause
		await get_tree().create_timer(next_spawn, false).timeout
	_spawn()
	
func _get_package_resource() -> PackageTemplate:
	var package : PackageTemplate
	if (_special_packages_with_time.size() > 0 && _special_packages_with_time[0].time < _get_day_time()):
		var special_package = _special_packages_with_time[0].package
		_special_packages_with_time.remove_at(0)
		return special_package
	else:
		var packages_priority = MissionManager.get_packages_with_priority(regular_packages)
		return LdHelper.get_random_from_weighted_array(packages_priority).package
		
func _next_time_for_spawn() -> float:
	var next_spawn = _time_between_spawns(GameState.current_day - 1)
	if (_special_packages_with_time.size() > 0):	
		next_spawn = min(_special_packages_with_time[0].time - _get_day_time() + 0.5, next_spawn) 
	return max(next_spawn, min_wait_between_spawns)

func _time_between_spawns(day_index : int) -> float:
	if (day_index < 0) || (day_index > GameState.max_days):
		return min_wait_between_spawns
	else:
		return time_between_spawns_per_day[day_index]

func _get_day_time() -> float:
	return _game_state.seconds_per_day - _game_state.time_remaining as float

func _calculate_special_packages_time(special_packages : Array[PackageTemplate]):
	var half_time = _game_state.seconds_per_day / 2
	var extra_packages = special_packages
	extra_packages.append_array(mission_packages)
	for extra_package in extra_packages:
		_special_packages_with_time.append({
			"time": _rng.randf_range(0, half_time),
			"package": extra_package 
		})
	_special_packages_with_time.sort_custom(func sort_by_time(a, b): return a.time > b.time)
	print("Special packages to spawn during the day: ")
	print(_special_packages_with_time.map(func to_string(a): 
		return "Time: " + str(a.time).pad_zeros(0) + ", package: " + a.package.resource_path))
