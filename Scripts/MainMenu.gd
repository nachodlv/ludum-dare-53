extends Node

var audio_player : FadeInOutAudio

func _ready():
	audio_player = await FadeInOutAudio.get_fadeinout_audio(self, "/MenuSound", load("res://SFX/Theme_Menu.mp3"))
	audio_player.fade_in(1)

func start_game():
	$AspectRatioContainer/Goal.visible = true
	
func do_play():
	audio_player.fade_out(2)
	GameState.start_game()
