@tool
extends Node2D

@export var packages : Array[PackageTemplate]
@export var spawner : PackageSpawner
@export var run : bool : 
	set (new_run):
		print(spawner)
		packages.clear()
		_load_packages_resourced("res://Packages/Regulars")
		get_node("../PackageSpawnerRoot").regular_packages = packages
		run = new_run

func _ready():
	packages.clear()
	if Engine.is_editor_hint():
		_load_packages_resourced("res://Packages/Regulars")
		spawner.regular_packages = packages
	
func _load_packages_resourced(path):
	var dir = DirAccess.open(path)
	if dir:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			file_name = path + "/" + file_name
			if dir.current_is_dir():
				_load_packages_resourced(file_name)
			else:
				var package = ResourceLoader.load(file_name) as PackageTemplate
				packages.append(package)
			file_name =  dir.get_next()
	else:
		print("An error occurred when trying to access the path.")
