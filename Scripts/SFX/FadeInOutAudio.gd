extends Node

class_name FadeInOutAudio

@onready var tween = get_tree().create_tween()
@onready var player : AudioStreamPlayer = $AudioStreamPlayer 

static func get_fadeinout_audio(tree_reference: Node, name: String, stream) -> FadeInOutAudio:
	await tree_reference.get_tree().process_frame
	var end_day_audio: FadeInOutAudio = tree_reference.get_tree().root.get_node("/root/GameState/" + name)
	if !end_day_audio:
		end_day_audio = load("res://Levels/FadeOutAudio.tscn").instantiate()
		end_day_audio.name = name
		await GameState.call_deferred("add_child", end_day_audio)
		await tree_reference.get_tree().process_frame
		end_day_audio.set_stream(stream)
	return end_day_audio
	

func set_stream(stream: AudioStream):
	if !player:
		player = $AudioStreamPlayer 
	player.stream = stream

func fade_in(fade_in_duration: float):
	tween = get_tree().create_tween()
	player.play()
	tween.tween_property(player, "volume_db", 0, fade_in_duration)
	
func fade_out(fade_out_duration: float):
	tween = get_tree().create_tween()
	tween.tween_property(player, "volume_db", -80, fade_out_duration)
	await tween.finished
	player.stop()
