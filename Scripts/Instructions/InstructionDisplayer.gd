extends Node

class_name InstructionDisplayer

@export var instruction_nodes : Array[NodePath]

var instructions : Dictionary = {
	0 : ["A little reminder! You can press 'P' for help"],
	1 : ["Check all the shipping information before sending the package!", "Remember! 'P' shows also were the details are!!"],
	2 : ["\nKeep going!\nI believe in you!", "Check all the shipping information before sending the package!"],
	3 : ["Tick, tock...\nThat's the sound of your life running out...", "Check all the shipping information before sending the package!"]
}

var instructions_font_size : Dictionary = {
	0 : [12],
	1 : [11, 12],
	2 : [12, 11],
	3 : [12, 11]
}

func set_displays(day_index):
	for i in range(instruction_nodes.size()):
		get_node(instruction_nodes[i]).visible = false
	
	
	if (!instructions.has(day_index)):
		return
	
	var instruction_texts = instructions[day_index]
	for i in range(instruction_nodes.size()):
		if i >= instruction_texts.size():
			break
		var target_node : Node = get_node(instruction_nodes[i])
		var label : Label = target_node.find_child("Label")
		label.text = instruction_texts[i]
		label.add_theme_font_size_override("font_size", instructions_font_size[day_index][i])
		target_node.visible = true


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
