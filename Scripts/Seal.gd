extends StaticBody2D

class_name Seal

static func try_cast(from_type):
	return from_type as Seal;

@export var id : int = 0
@export var sprite : Sprite2D
var original_position : Vector2

func _ready():
	original_position = global_transform.origin	
	
func return_to_start():
	global_transform.origin = original_position
