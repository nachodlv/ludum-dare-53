extends Mission

class_name DailyMission

@export var banned_origin_place : String
@export var banned_person : String
@export var reward_label : String
@export var score_given : int

var daily_mission_state = load("res://Levels/Mission/daily_mission_state.tscn")

func generate_state():
	return daily_mission_state
