extends Node

signal missions_updated(current_missions : Array[Mission])
signal mission_status_update(mission : Mission, status, index : int)

var current_missions : Array[Mission]
var _game_state : GameState
var _last_day : int = 0

static func get_current(contextNode: Node):
	return contextNode.get_node("/root/MissionManager")

func _ready():
	_game_state = GameState.get_current(self)
	_day_change(_game_state.current_day)
	_game_state.day_change.connect(_day_change)

func _day_change(new_day: int):
	if _last_day == new_day:
		return
	_last_day = new_day
	if _game_state.events_per_day.events.size() >= _game_state.current_day:
		var new_missions = _game_state.events_per_day.events[_game_state.current_day - 1].mission_start as Array[Mission]
		for new_mission in new_missions:
			var mission_state = new_mission.generate_state().instantiate() as MissionState
			await self.call_deferred("add_child", mission_state)
			mission_state.mission = new_mission
		current_missions.append_array(new_missions)
		missions_updated.emit(current_missions)

func get_packages_with_priority(packages: Array[PackageTemplate]) -> Array :
	var result : Array
	for package in packages:
		var extra_weight = false
		for child in get_children():
			if child is DailyMissionState:
				if child.package_matches(package):
					extra_weight = true
					break
		var weight = 3 if extra_weight else 1
		
		if package.has_empty_values():
			weight = 0 if GameState.current_day == 1 else 0.5
		
		result.append({
			"package": package,
			"weight": weight
		})
	return result
	
func update_mission_status(mission : Mission, status):
	var index = current_missions.find(mission) as int
	mission_status_update.emit(mission, status, index)

func reset():
	current_missions = []
	_last_day = 0
