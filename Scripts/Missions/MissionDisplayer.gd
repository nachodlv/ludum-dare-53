extends Node

func _ready():
	_update_missions(MissionManager.current_missions)
	MissionManager.missions_updated.connect(_update_missions)
	MissionManager.mission_status_update.connect(_update_mission_status)

func _update_missions(current_missions : Array[Mission]):
	var mission_post_its = get_children() as Array[Node]
	for i in range(0, mission_post_its.size()):
		var post_it = mission_post_its[i] as Node
		if i < current_missions.size():
			post_it.visible = true
			post_it.get_node("Label").text = current_missions[i].post_it
		else:
			post_it.visible = false
		
func _update_mission_status(mission: Mission, status, index : int):
	var mission_post_its = get_children() as Array[Node]
	var sprite = mission_post_its[index].get_node("Sprite2D") as Sprite2D
	var color = sprite.modulate as Color
	if status.failed:
		color = Color.DARK_RED
	elif status.finished:
		color = Color.FOREST_GREEN
	sprite.modulate = color
