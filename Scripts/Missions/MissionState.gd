extends Node

class_name MissionState

var mission : Mission

func finish_mission():
	MissionManager.current_missions.erase(mission)
	queue_free()
