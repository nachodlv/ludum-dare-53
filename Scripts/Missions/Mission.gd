extends Resource

class_name Mission

@export var post_it : String

var mission_state = load("res://Levels/Mission/boss_mission_state.tscn")

func generate_state():
	return mission_state
