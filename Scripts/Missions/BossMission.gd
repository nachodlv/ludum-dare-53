extends Mission

class_name BossMission

@export var starting_package : PackageTemplate
@export var sad_timmy : PackageTemplate
@export var happy_timmy : PackageTemplate
@export var happy_boss : String
@export var angry_boss_package_sent : String
@export var angry_boss_package_destroyed: String
@export var extra_score : float

var boss_mission_state = load("res://Levels/Mission/boss_mission_state.tscn")

func generate_state():
	return boss_mission_state
