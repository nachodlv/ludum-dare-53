extends MissionState

class_name DailyMissionState

var daily_mission : DailyMission
var failed : bool = false

func _ready():
	daily_mission = mission as DailyMission
	GameState.package_sent.connect(_package_sent)
	GameState.day_ended.connect(_day_ended)
	
func _package_sent(package : Package):
	if failed:
		return
	failed = package_matches(package.package)
	if failed:
		MissionManager.update_mission_status(daily_mission, {
			"failed": true,
			"finished": false
		})
		
func package_matches(package : PackageTemplate) -> bool:
	var banned_origin = daily_mission.banned_origin_place != "" as bool
	if banned_origin && package.origin == daily_mission.banned_origin_place:
		return true
	
	var banned_person = daily_mission.banned_person != "" as bool
	var matches_person = package.from == daily_mission.banned_person || package.to == daily_mission.banned_person
	if banned_person && matches_person:
		return true
		
	return false

func _day_ended():
	if !failed:
		GameState.add_mission_reward({
			"label": daily_mission.reward_label,
			"score": daily_mission.score_given
		})
	finish_mission()
