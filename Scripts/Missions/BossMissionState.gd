extends MissionState

class_name BossMissionState

enum BossState {
	START,
	SENT,
	DESTROYED,
	SHELVED
}

var boss_mission : BossMission
var mission_state : BossState = BossState.START:
	set (new_state):
		if mission_state != new_state:
			mission_state = new_state
			update_status()

func _ready():
	boss_mission = mission as BossMission
	GameState.package_sent.connect(_package_sent)
	GameState.package_destroyed.connect(_package_detroyed)
	GameState.package_shelved.connect(_package_shelved)
	GameState.day_change.connect(_day_started)
	GameState.day_ended.connect(_day_ended)

func _package_sent(package : Package):
	if boss_mission.starting_package == package.package:
		mission_state = BossState.SENT
		
func _package_detroyed(package : Package):
	if boss_mission.starting_package == package.package:
		mission_state = BossState.DESTROYED
		
func _package_shelved(package: Package):
	if boss_mission.starting_package == package.package:
		mission_state = BossState.SHELVED

func _day_started(new_day : int):
	var spawner = get_node("/root/Node2D/PackageSpawnerRoot") as PackageSpawner
	if mission_state == BossState.SENT:
		spawner.mission_packages.append(boss_mission.happy_timmy)
		finish_mission()
	elif mission_state == BossState.DESTROYED || mission_state == BossState.SHELVED:
		spawner.mission_packages.append(boss_mission.sad_timmy)
		finish_mission()
		
func _day_ended():
	if mission_state == BossState.START:
		return
		
	var boss_message = boss_mission.happy_boss
	if mission_state == BossState.DESTROYED:
		boss_message = boss_mission.angry_boss_package_destroyed
	elif mission_state == BossState.SENT:
		boss_message = boss_mission.angry_boss_package_sent
	get_node("/root/EndOfDay/BossMessage").visible = true
	get_node("/root/EndOfDay/BossMessage/Label").text = boss_message
	
	if mission_state == BossState.SHELVED:
		GameState.add_mission_reward({
			"label": "Performance bonus:",
			"score": boss_mission.extra_score
		})
	MissionManager.current_missions.erase(mission)

func update_status():
	MissionManager.update_mission_status(boss_mission, {
			"failed": BossState.SHELVED != mission_state,
			"finished": BossState.SHELVED == mission_state
		})
