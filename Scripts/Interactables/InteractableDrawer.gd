extends InteractableArea

class_name InteractableDrawer

@export var hover_time_required : float = 0.3
@export var has_hover : bool = false :
	set (new_hover):
		if (new_hover != has_hover):
			has_hover = new_hover
			current_hover_time = 0
		

signal on_drawer_open
signal on_drawer_close

var current_hover_time : float = 0

static func try_cast(from_type):
	return from_type as InteractableDrawer;

func _process(delta):
	if (has_hover):
		current_hover_time += delta
		
		if (current_hover_time > hover_time_required):
			set_status(true)
			
	if (enabled):
		($Sprite as Sprite2D).texture = load("res://Sprites/Boss_2.png")
	elif (has_hover):
		($Sprite as Sprite2D).texture = load("res://Sprites/Boss_medio.png")
	else:
		($Sprite as Sprite2D).texture = load("res://Sprites/Boss_1.png")
		
		
func set_status(status : bool):
	if (status != enabled):
		enabled = status
		if (status):
			on_drawer_open.emit()
		else:
			current_hover_time = 0
			on_drawer_close.emit()
		
func shelve_object(target_object):
	super(target_object)
	set_status(false)
	
