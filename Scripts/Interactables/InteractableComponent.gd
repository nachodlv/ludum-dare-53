extends Node

class_name InteractableComponent

@export var interaction_shape : Shape2D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func is_overlapping_any_area(trace_position : Vector2):
	var areas = get_overlapping_areas(trace_position)
	var is_overlapping = !areas.is_empty()
	return is_overlapping
	
func is_overlapping_any_package(trace_position:Vector2):
	var areas = get_overlapping_areas(trace_position, true, true)
	var is_overlapping = !areas.is_empty()
	if (is_overlapping && areas.any(_is_package)):
		return true;
	
	return false;

func _is_package(area):
	return area.collider is Package

func get_interactable_atpos(trace_position : Vector2, custom_class_filters, layer_mask : int = 0xFFFFFFFF):
	var areas = get_overlapping_areas(trace_position, true, true, layer_mask)
	
	areas.sort_custom(func(lhs,rhs): 
		return (lhs.collider.position - trace_position).length() < (rhs.collider.position - trace_position).length())
	
	for i in range(areas.size()):
		for j in range(custom_class_filters.size()):
			if custom_class_filters[j].try_cast(areas[i].collider):
				return areas[i].collider
	
func get_overlapping_areas(trace_position : Vector2, check_areas : bool = true, check_bodies : bool = false, layer_mask : int = 0xFFFFFFFF):
	var space_state = get_viewport().find_world_2d().direct_space_state
	var query_shape = get_trace_area(trace_position, check_areas, check_bodies, layer_mask)
	return space_state.intersect_shape(query_shape)
	
func get_trace_area(trace_position : Vector2, check_areas : bool = true, check_bodies : bool = false, layer_mask : int = 0xFFFFFFFF):
	var query_shape = PhysicsShapeQueryParameters2D.new()
	query_shape.collide_with_areas = check_areas
	query_shape.collide_with_bodies = check_bodies
	query_shape.shape = interaction_shape
	query_shape.collision_mask = layer_mask;
	
	query_shape.transform = Transform2D(0, trace_position)
	return query_shape
