extends Area2D

class_name InteractableArea

signal area_interacted
signal wrong_package

enum AreaType {AREA_NONE, AREA_MAIN, AREA_SEND, AREA_GARBAGE, AREA_SHELVE}

@export var area_type : AreaType
@export var enabled : bool = true

static func try_cast(from_type):
	return from_type as InteractableArea;

# Called when the node enters the scene tree for the first time.
func _ready():
	if is_main():
		GameState.main_area = (self as InteractableArea)
	pass # Replace with function body.

func is_main():
	return area_type == AreaType.AREA_MAIN

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func solve_interaction(target_object = null) -> bool:
	if (!enabled || target_object == null):
		return false
	match area_type:
		AreaType.AREA_MAIN:
			release_object(target_object)
			return false
		AreaType.AREA_SEND:
			send_object(target_object)
		AreaType.AREA_SHELVE:
			shelve_object(target_object)
		AreaType.AREA_GARBAGE:
			destroy_object(target_object)
	return true

func send_object(target_object):
	#TODO: validate agaisnt currency/mission logic (wheter to add or not)
	if (target_object is Package) && target_object.should_be_sent(GameState.current_day):
		GameState.send_package(target_object)
		$AudioStreamPlayer.play()
	elif target_object is Package:
		GameState.send_wrong_package(target_object)
		wrong_package.emit()
	
	target_object.queue_free()
	area_interacted.emit()
	pass

func release_object(target_object):
	pass

func shelve_object(target_object):
	if target_object is Package:
		GameState.shelve_package(target_object)
	target_object.queue_free()
	area_interacted.emit()
	pass

func destroy_object(target_object):
	if target_object is Package:
		GameState.destroy_package(target_object)
		area_interacted.emit()
	target_object.queue_free()
	pass
