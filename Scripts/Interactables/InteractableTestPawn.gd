extends Sprite2D

class_name InteractableTestingPawn

@export var interactable_comp : InteractableComponent
@export var holding_package : RigidBody2D

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var mouse_pos = get_global_mouse_position()
	position = mouse_pos
	
	if interactable_comp.is_overlapping_any_area(position):
		modulate = Color.AQUAMARINE
	else:
		modulate = Color.DARK_RED
	
	if holding_package != null:
		holding_package.position = position
	pass

func _unhandled_input(event):
	if event is InputEventMouse and event.is_pressed():
		print("Mouseclick")
		var found_area = interactable_comp.get_interactable_atpos(position, [InteractableArea], 4)
		if (found_area != null) and (found_area is InteractableArea):
			print((found_area as InteractableArea).get_collision_mask_value(found_area.collision_mask))
			found_area.solve_interaction(holding_package)
	pass
