extends RigidBody2D

class_name Package

@export var package_resource : Resource
@export var sealing_distance : float = 40

static func try_cast(from_type):
	return from_type as Package;

signal on_pickup (package : Package)
signal correct_seal_signal
signal incorrect_seal

var _showingFront = true
var package : PackageTemplate
var required_ids : int = 0
var completed_seals : int = 0
var index_to_seal : Dictionary
var original_collision_size : Vector2
var rb_physics_enabled : bool = false

@export var sprite : Sprite2D
@export var left_hand_pivot : Node2D
@export var right_hand_pivot : Node2D

func _ready():
	_update_package()
	index_to_seal = {0: $Back/RedSealFilled, 1: $Back/BlueSealFilled, 2: $Back/GreenSealFilled}

func _physics_process(delta):
	if (rb_physics_enabled):
		move_and_collide(linear_velocity)

func save_original_collision_size():
	original_collision_size = $CollisionShape2D.shape.get_rect().size

func _update_package():
	if !package:
		return
	$Front/FromText.text = "From " + package.from
	$Front/ToText.text = "To " + package.to
	#$PackageSprite.modulate = package.color
	$Front/PaqueteFrontV002.modulate = package.color
	$Back/PostItText.text = package.post_it 
	$Back/PaqueteTurnaroundV002.modulate = package.color
	$Front/Origin.text = package.origin
	$Front/Number.text = str(package.shipping_number) if package.shipping_number != 0 else ""
	var rnd = RandomNumberGenerator.new()
	# On the first day we only want 2 seals. For the other days we want 3 seals
	required_ids = rnd.randi_range(0, 7)
	if package.timmy_package || package.mafia_hand:
		required_ids = 0
	elif GameState.current_day == 1:
		required_ids = rnd.randi_range(0, 3)
	var red_seal = (required_ids & 1 << 0) != 0
	var blue_seal = (required_ids & 1 << 1) != 0
	var green_seal = (required_ids & 1 << 2) != 0
	print(required_ids)
	$Back/RedSeal.visible = red_seal;
	$Back/BlueSeal.visible = blue_seal;
	$Back/GreenSeal.visible = green_seal;
	
func revert():
	_showingFront = !_showingFront
	$Front.visible = _showingFront
	$Back.visible = !_showingFront
	
func try_apply_seal(seal : Seal) -> bool:
	if !_showingFront:
		_apply_seal(seal)
		return true
	return false
	
func _apply_seal(seal : Seal):
	var seal_pos = seal.global_transform.origin
	
	var seal_index = roundi(log(seal.id) / log(2))
	var filled = index_to_seal[seal_index]
	
	var distance = filled.global_transform.origin.distance_to(seal_pos)
	var correct_seal = (seal.id & required_ids) != 0
	var correct_position = distance <= sealing_distance
	var dont_have_it_yet = (completed_seals & seal.id) == 0
	
	var should_instance_new = false
	
	if (correct_seal):
		if (correct_position && dont_have_it_yet):
			filled.visible = true
			completed_seals |= seal.id
			correct_seal_signal.emit()
		else:
			should_instance_new = true
			
	if (!correct_seal):
		completed_seals |= seal.id
		should_instance_new = true
	
	if (should_instance_new):
		var new_seal = filled.duplicate()
		$Back.add_child(new_seal)
		new_seal.global_transform.origin = seal_pos
		new_seal.visible = true
		incorrect_seal.emit()
	
func was_sealed():
	return completed_seals == required_ids
	
func get_size() -> float:
	return $CollisionShape2D.shape.get_rect().size.x
	
func set_size(size_scalar : float):
	#var previous_transform = global_transform;
	#var new_transform = Transform2D(previous_transform.get_rotation(), Vector2(size_scalar, size_scalar), previous_transform.get_skew(), previous_transform.get_origin())
	scale = Vector2(size_scalar, size_scalar)
	($CollisionShape2D.shape as RectangleShape2D).size = original_collision_size * size_scalar

func get_seal_count() -> int:
	var count = 0
	for i in range(2):
		if ((1 << i & required_ids) != 0):
			count+=1
	return count
	
func should_be_sent(day_index : int):
	var default_condition = !package.to.is_empty() && was_sealed()
	if day_index < 1:
		return default_condition
	else: 
		return default_condition && !package.has_empty_values()
