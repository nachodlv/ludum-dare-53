extends Node

class_name EndOfDayScreen

var end_day_audio : FadeInOutAudio

func _ready():
	
	start_audio()
	
	var label = $AspectRatioContainer/TextureRect/EndOfDayLabel
	label.text = str(GameState.current_day).pad_zeros(3)
	var today_currency = $AspectRatioContainer/TextureRect/VBoxContainer/TodayCurrency/Currency
	var today_discounts = $AspectRatioContainer/TextureRect/VBoxContainer/TodayDiscounts/Currency
	var today_discounts_container = $AspectRatioContainer/TextureRect/VBoxContainer/TodayDiscounts
	var objective = $AspectRatioContainer/TextureRect/VBoxContainer/Objective/Currency
	
	today_currency.text = _display_money(GameState.day_score)
	
	today_discounts_container.visible = GameState.day_discounts != 0
	today_discounts.text = _display_money(GameState.day_discounts)
	objective.text =  _display_money(GameState.score_objective)
	
	_display_missions_rewards(null)
	GameState.mission_added.connect(_display_missions_rewards)
	_update_total()
	
	var remaining_days = $AspectRatioContainer/TextureRect/DaysRemaingLabel
	var days_remaining = GameState.max_days - GameState.current_day
	remaining_days.text = str(days_remaining) + " day" + ("s" if days_remaining > 1 else "") + " remaining"
	
	if days_remaining == 0:
		remaining_days.visible = false
	
func load_new_day_scene():
	if GameState.max_days - GameState.current_day <= 0:
		$AspectRatioContainer/Control.visible = true
		var win = GameState.score_objective <= GameState.total_score
		$AspectRatioContainer/Control/WinScreen.visible = win
		$AspectRatioContainer/Control/LooseScreen.visible = !win
	else:
		stop_audio()
		GameState.start_day()	

func _display_money(score : float) -> String:
	var display_sign = "-$" if (score < 0) else "$"
	var str_score = str(abs(score))
	var mod = str_score.length() % 3
	var result : String
	for i in range(0, str_score.length()):
		if i != 0 && i % 3 == mod:
			result += ","
		result += str_score[i]
	return display_sign + str_score
	
func _display_missions_rewards(_mission_reward):
	var start_mission_reward = 2
	var end_mission_reward = 3
	var vertical_container = $AspectRatioContainer/TextureRect/VBoxContainer
	var children = vertical_container.get_children()
	for i in range(0, GameState.missions_rewards.size()):
		var mission_reward = GameState.missions_rewards[i]
		children[i + start_mission_reward].get_node("Label").text = mission_reward.label
		children[i + start_mission_reward].visible = true
		children[i + start_mission_reward].get_node("Currency").text = _display_money(mission_reward.score)
	_update_total()
	
func _update_total():
	var total = $AspectRatioContainer/TextureRect/VBoxContainer/Total/Currency
	total.text = _display_money(GameState.total_score)
	
func _main_menu():
	GameState.reset()
	stop_audio()
	get_tree().change_scene_to_file("res://Levels/main_menu.tscn")

func start_audio():
	await get_tree().process_frame
	end_day_audio = await FadeInOutAudio.get_fadeinout_audio(self, "EndOfDayAudio", load("res://SFX/Theme_Base.mp3"))
	end_day_audio.fade_in(1)
	
func stop_audio():
	end_day_audio.fade_out(3)
	
