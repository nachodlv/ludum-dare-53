extends Node

func _ready():
	var game_state = GameState.get_current(self) as GameState
	_update_score_text(0, game_state.total_score, 0)
	game_state.score_change.connect(_update_score_text)
	game_state.wrong_package_sent.connect(show_negative_feedback)

func _update_score_text(old_score : float, new_score : float, discounts: float):
	var current_score = new_score + discounts
	$Label.text = str(current_score).pad_zeros(3 if current_score < 0 else 4)
	
func show_negative_feedback(discounts : float):
	var tween = get_tree().create_tween()
	tween.tween_property($Label, "modulate", Color.RED, 0.3)
	await tween.finished
	await 0.2
	var new_tween = get_tree().create_tween()
	new_tween.tween_property($Label, "modulate", Color.WHITE, 0.5)
