extends Node

var _game_state : GameState
var _timer_sound_reproduced  = false

func _ready():
	_game_state = GameState.get_current(self)

func _process(delta):
	$Label.text = str(_game_state.time_remaining).pad_decimals(0) + "s"
	if _game_state.time_remaining < 5 && !_timer_sound_reproduced:
		$AudioStreamPlayer.play()
		_timer_sound_reproduced = true
	
