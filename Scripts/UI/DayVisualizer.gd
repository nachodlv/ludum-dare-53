extends Node

func _ready():
	_update_day(GameState.current_day)
	GameState.day_change.connect(_update_day)
	
func _update_day(new_day : int):
	$Label.text = "Day " + str(new_day)
