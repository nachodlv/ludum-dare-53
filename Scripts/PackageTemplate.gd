extends Resource

class_name PackageTemplate

@export var from : String = ""
@export var to : String = ""
@export var color : Color = Color.WHITE
@export var post_it : String = ""
@export var shipping_number : int = 0
@export var origin : String = ""
@export var tags : Array[String] = []
@export var timmy_package = false
@export var mafia_hand = false
@export var hogwarts = false


func has_empty_values():
	return from.is_empty() || to.is_empty() || origin.is_empty() || shipping_number == 0
