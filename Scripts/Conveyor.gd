extends Node2D

class_name Conveyor

@export var moving_speed = 150 as float
@export var max_slots = 5
@export var space_between_packages = 15
@export var package_conveyor_size = 0.7

var _moving_packages : Array[Package]
var _package_size : float
var _layer_package = 2 as int

func _process(delta):
	for i in range(_moving_packages.size()):
		var package = _moving_packages[i]
		var move_position_x = $To.global_transform.origin.x - _package_size * i - _package_size / 2
		var position = package.global_transform.origin
		if (abs(move_position_x - position.x) < 10):
			continue
		var direction = (Vector2(move_position_x, position.y) - position).normalized()
		package.translate(direction * moving_speed * delta)

func add_package_to_conveyor(package: Package):
	if (_package_size < 1):
		_package_size = package.get_size() * package_conveyor_size + space_between_packages
		
	package.set_size(package_conveyor_size)
	_moving_packages.append(package)
	#package.set_collision_layer_value(_layer_package, false)
	#package.set_collision_mask_value(_layer_package, false)
	#package.set_collision_layer_value(4, true)
	#package.set_collision_mask_value(4, true)
	package.freeze = true
	package.global_transform.origin = global_transform.origin
	package.on_pickup.connect(remove_package)
		
func remove_package(package: Package):
	package.set_size(1)
	package.on_pickup.disconnect(remove_package)
	#package.set_collision_layer_value(_layer_package, true)
	#package.set_collision_mask_value(_layer_package, true)
	#package.set_collision_layer_value(4, false)
	#package.set_collision_mask_value(4, false)
	package.freeze = false	
	_moving_packages.erase(package)
	
func has_available_slots() -> bool :
	return _moving_packages.size() < max_slots

